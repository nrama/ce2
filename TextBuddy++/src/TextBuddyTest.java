import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class TextBuddyTest {
	private static final String FILE_NAME = "testFile.txt";
	private static final String FIRST_TEXT = "add cats are running";
	private static final String SECOND_TEXT = "add apples are red";
	private static final String THIRD_TEXT = "add bees are buzzing";
	private static final String FOURTH_TEXT = "add cats are leaping";
	private static final String FIFTH_TEXT = "add cats are sleeping";
	private static final String SEARCH_KEY = "search cat";
	private static final int FOUND_CAT1 = 1;
	private static final int FOUND_CAT2 = 4;
	private static final int FOUND_CAT3 = 5;

	@Test
	public void testExecuteSort() {

		ArrayList<String> expectedArray = new ArrayList<String>();

		TextBuddy.executeAdd(FILE_NAME, SECOND_TEXT, expectedArray);
		TextBuddy.executeAdd(FILE_NAME, THIRD_TEXT, expectedArray);
		TextBuddy.executeAdd(FILE_NAME, FIRST_TEXT, expectedArray);

		ArrayList<String> actualArray = new ArrayList<String>();

		TextBuddy.executeAdd(FILE_NAME, FIRST_TEXT, actualArray);
		TextBuddy.executeAdd(FILE_NAME, SECOND_TEXT, actualArray);
		TextBuddy.executeAdd(FILE_NAME, THIRD_TEXT, actualArray);

		TextBuddy.executeSort(FILE_NAME, actualArray);

		assertEquals(TextBuddy.executeDisplay(FILE_NAME, expectedArray),
				TextBuddy.executeDisplay(FILE_NAME, actualArray));
	}

	@Test
	public void testExecuteSearchNotFound() {

		ArrayList<String> taskList = new ArrayList<String>();

		TextBuddy.executeAdd(FILE_NAME, SECOND_TEXT, taskList);
		TextBuddy.executeAdd(FILE_NAME, THIRD_TEXT, taskList);

		ArrayList<Integer> noKeyFound = new ArrayList<Integer>(); // expected
																	// empty
																	// array

		assertEquals(noKeyFound, TextBuddy.executeSearchAndDisplay(FILE_NAME,
				SEARCH_KEY, taskList));
	}

	@Test
	public void testExecuteSearchFound() {

		ArrayList<String> taskList = new ArrayList<String>();

		TextBuddy.executeAdd(FILE_NAME, FIRST_TEXT, taskList);
		TextBuddy.executeAdd(FILE_NAME, SECOND_TEXT, taskList);
		TextBuddy.executeAdd(FILE_NAME, THIRD_TEXT, taskList);
		TextBuddy.executeAdd(FILE_NAME, FOURTH_TEXT, taskList);
		TextBuddy.executeAdd(FILE_NAME, FIFTH_TEXT, taskList);

		// expected array, key found in respective lines
		ArrayList<Integer> keyFound = new ArrayList<Integer>();
		keyFound.add(FOUND_CAT1);
		keyFound.add(FOUND_CAT2);
		keyFound.add(FOUND_CAT3);

		assertEquals(keyFound, TextBuddy.executeSearchAndDisplay(FILE_NAME,
				SEARCH_KEY, taskList));
	}

}