/**
 * This is a CLI (Command Line Interface) program called TextBuddy which uses java to manipulate
 * text in a file. The file will be stored locally.
 * 
 * This program assumes that the user is entering the correct input at all times.
 * 
 * The following commands can be executed by TextBuddy (not case-sensitive):
 * - Add
 * - Delete
 * - Display
 * - Sort
 * - Search
 * - Clear
 * - Exit
 * 
 * @author - Rama 
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class TextBuddy {

	public static final int LENGTH_OF_ADDCMD = 4;
	public static final int LENGTH_OF_DELCMD = 7;
	public static final int LENGTH_OF_SEARCHCMD = 7;

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		if (args.length > 0) {
			String fileName = args[0];
			ArrayList<String> taskList = taskListArray();
			welcomeMsg(fileName);
			readFile(fileName, createFile(fileName), taskList);

			while (true) {
				promptCmd();
				executeCommand(fileName, createFile(fileName),
						readUserCommand(), taskList);
			}
		}
	}

	private static File createFile(String fileName) {
		File newFile = new File(fileName);
		return newFile;
	}

	private static ArrayList<String> taskListArray() {
		ArrayList<String> taskList = new ArrayList<String>();
		return taskList;
	}

	private static void welcomeMsg(String fileName) {
		showToUser("Welcome to Textbuddy. " + fileName + " is ready for use");
	}

	private static void promptCmd() {
		System.out.print("command: ");
	}

	private static String readUserCommand() {
		return sc.nextLine();
	}

	private static void showToUser(String str) {
		System.out.println(str);
	}

	private static void readFile(String fileName, File myFile,
			ArrayList<String> taskList) {

		String line;

		try {
			FileReader fr = new FileReader(myFile);
			BufferedReader input = new BufferedReader(fr);

			while ((line = input.readLine()) != null) {
				taskList.add(line);
			}

			input.close();
		} catch (IOException e) {
			createFile(fileName);
		}
	}

	private static void executeSave(ArrayList<String> taskList, File myFile) {

		try {
			FileWriter fw = new FileWriter(myFile);
			BufferedWriter output = new BufferedWriter(fw);

			for (int i = 0; i < taskList.size(); i++) {
				output.write(taskList.get(i));
				output.newLine();
			}

			output.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	/**
	 * This method executes each command and saves after the user exits. The
	 * commands are not case-sensitive.
	 */
	private static void executeCommand(String fileName, File myFile,
			String userCommand, ArrayList<String> taskList) {

		if (userCommand.toLowerCase().startsWith("add ".toLowerCase())) {
			executeAdd(fileName, userCommand, taskList);
		}

		if (userCommand.toLowerCase().startsWith("delete ".toLowerCase())) {
			executeDel(fileName, userCommand, taskList);
		}

		if (userCommand.toLowerCase().startsWith("clear".toLowerCase())) {
			executeClear(fileName, taskList);
		}

		if (userCommand.toLowerCase().startsWith("display".toLowerCase())) {
			executeDisplay(fileName, taskList);
		}

		if (userCommand.toLowerCase().startsWith("sort".toLowerCase())) {
			executeSort(fileName, taskList);
		}

		if (userCommand.toLowerCase().startsWith("search ".toLowerCase())) {
			executeSearchAndDisplay(fileName, userCommand, taskList);
		}

		if (userCommand.toLowerCase().startsWith("exit".toLowerCase())) {
			executeSave(taskList, myFile);
			System.exit(0);
		}
	}

	public static void executeAdd(String fileName, String addCmd,
			ArrayList<String> taskList) {

		String task = addCmd.substring(LENGTH_OF_ADDCMD);
		taskList.add(task);

		showToUser("added to " + fileName + ": \"" + task + "\"");
	}

	private static void executeDel(String fileName, String delCmd,
			ArrayList<String> taskList) {

		String delTask = delCmd.substring(LENGTH_OF_DELCMD);
		Integer delIndex = Integer.valueOf(delTask);

		showToUser("deleted from " + fileName + ": \""
				+ taskList.get(delIndex - 1) + "\"");
		taskList.remove(delIndex - 1);
	}

	private static void executeClear(String fileName, ArrayList<String> taskList) {

		taskList.clear();
		showToUser("all content deleted from " + fileName);
	}

	public static ArrayList<String> executeDisplay(String fileName,
			ArrayList<String> taskList) {

		if (taskList.isEmpty())
			showToUser(fileName + " is empty");

		else {
			for (int i = 0; i < taskList.size(); i++) {
				showToUser(+i + 1 + ". " + taskList.get(i));
			}
		}
		return taskList;
	}

	public static void executeSort(String fileName, ArrayList<String> taskList) {

		int i;
		boolean flag = true;
		String temp;

		while (flag) {
			flag = false;

			for (i = 0; i < taskList.size() - 1; i++) {

				// ascending sort
				if (taskList.get(i).compareToIgnoreCase(taskList.get(i + 1)) > 0) {
					temp = taskList.get(i);
					taskList.set(i, taskList.get(i + 1)); // swapping
					taskList.set(i + 1, temp);
					flag = true;
				}
			}
		}
		showToUser(fileName + " has been sorted alphabetically.");
	}

	public static ArrayList<Integer> executeSearchAndDisplay(String fileName,
			String searchCmd, ArrayList<String> taskList) {

		String searchKey = searchCmd.substring(LENGTH_OF_SEARCHCMD);
		String searchKeyDisplay = "\"" + searchKey + "\"";

		ArrayList<Integer> foundLine = new ArrayList<Integer>();

		for (int i = 0; i < taskList.size(); i++) {
			if (taskList.get(i).contains(searchKey))
				foundLine.add(i + 1);
		}

		if (foundLine.isEmpty()) {
			showToUser(searchKeyDisplay + " was not found");

			return foundLine;
		}

		else if (foundLine.size() == 1) {
			showToUser(searchKeyDisplay + " was found in line "
					+ foundLine.get(0) + ": ");
			showToUser(+foundLine.get(0) + ". "
					+ taskList.get(foundLine.get(0) - 1));

			return foundLine;
		}

		else {
			System.out.print(searchKeyDisplay + " was found in lines ");
			for (int j = 0; j < foundLine.size() - 1; j++) {
				System.out.print(foundLine.get(j) + ", ");
			}
			showToUser(foundLine.get(foundLine.size() - 1) + ": ");

			for (int k = 0; k < foundLine.size(); k++) {
				showToUser(+foundLine.get(k) + ". "
						+ taskList.get(foundLine.get(k) - 1));
			}
			return foundLine;
		}

	}
}